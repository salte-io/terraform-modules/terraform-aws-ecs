# Terraform Module: ECS (Fargate)

> Simplified ECS (Fargate) Deployments within Salte!

## Table of Contents

* [Dependencies](#dependencies)
* [Usage](#usage)
  * [Input Variables](#input-variables)
  * [Output Variables](#output-variables)
* [Author Information](#author-information)

## Dependencies

This module depends on a correctly configured [AWS Provider](https://www.terraform.io/docs/providers/aws/index.html) in your Terraform codebase.

## Usage

```tf
# ...
data "aws_route53_zone" "default" {
  name = "quillie.net."
}

module "certificate" {
  source  = "git::https://gitlab.com/salte-io/terraform-modules/terraform-aws-certificate.git?ref=1.0.2"

  zone_id = "${data.aws_route53_zone.default.zone_id}"
  domain_name = "api.alpha.quillie.net"
}

module "ecs" {
  source = "git::https://gitlab.com/salte-io/terraform-modules/terraform-aws-ecs.git?ref=1.1.2"

  name        = "quillie-api"
  environment = "alpha"
  domain_name = "api.alpha.quillie.net"

  certificate_arn = module.certificate.certificate_arn
}
```

Then, load the module using `terraform init`.

### Input Variables

Available variables are listed below, along with their default values:

| **Variable** | **Description** | **Required?** |
| -------- | ----------- | ------- |
| `name` | The name of the application. | **Yes** |
| `environment` | The current environment. | **Yes** |
| `certificate_arn` | The ARN of the Certificate in AWS. | **Yes** |
| `domain_name` | The domain to associate the service to | **Yes** |
| `health_check` | The health check path location | No |
| `image_tag` | The image tag for the docker image | No |
| `environment_variables` | Environment Variables to provide to the Service | No |
| `secret_variables` | Systems Manager Parameter ARNs to provide to the Service | No |
| `port` | The port the application runs on, this is also provided to the application as the `PORT` environment variable. | No (defaults to 8080) |

### Output Variables

Available output variables are listed below:

| **Variable** | **Description** |
| -------- | ----------- |
| `zone_id` | The zone id of the load balancer. |
| `dns_name` | The dns name of the load balancer. |

## Author Information

This module is currently maintained by the individuals listed below.

* [Nick Woodward](https://github.com/nick-woodward)
