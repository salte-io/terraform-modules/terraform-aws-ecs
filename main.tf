locals {
  full_name = var.environment == "live" ? var.name : "${var.name}-${var.environment}"
  vpc_name  = "salte-public"

  image = "${aws_ecr_repository.default.repository_url}:${var.image_tag}"

  environment_variables = [{
    name  = "PORT",
    value = var.port
    }, {
    name  = "ENVIRONMENT",
    value = var.environment
  }]

  external_environment_variables = [for name in keys(var.environment_variables) : {
    name = name
    value = lookup(var.environment_variables, name)
  }]

  external_secret_variables = [for name in keys(var.secret_variables) : {
    name = name
    valueFrom = lookup(var.secret_variables, name)
  }]

  tags = {
    application = var.name
    environment = var.environment
    terraform   = "true"
  }
}

data "aws_vpc" "default" {
  tags = {
    Name = local.vpc_name
  }
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    Name = local.vpc_name
  }
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_lb" "default" {
  name = "salte-global"
}

data "aws_lb_listener" "default" {
  load_balancer_arn = data.aws_lb.default.arn
  port              = 443
}

resource "aws_lb_listener_rule" "default" {
  listener_arn = data.aws_lb_listener.default.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.default.arn
  }

  condition {
    host_header {
      values = [var.domain_name]
    }
  }
}

resource "aws_lb_target_group" "default" {
  name        = local.full_name
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default.id
  target_type = "ip"

  health_check {
    path = var.health_check
  }
}

resource "aws_ecs_cluster" "default" {
  name = local.full_name
}

resource "aws_ecs_task_definition" "default" {
  family                   = local.full_name
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = "arn:aws:iam::853196007801:role/salte-ecs"

  container_definitions = <<DEFINITION
  [{
    "name": "${local.full_name}",
    "cpu": 256,
    "essential": true,
    "image": "${local.image}",
    "memory": 512,
    "mountPoints": [],
    "networkMode": "awsvpc",
    "portMappings": [{
      "containerPort": ${var.port},
      "hostPort": ${var.port},
      "protocol": "tcp"
    }],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${var.name}",
        "awslogs-region": "us-east-1",
        "awslogs-stream-prefix": "${var.environment}"
      }
    },
    "environment": ${jsonencode(concat(local.environment_variables, local.external_environment_variables))},
    "secrets": ${jsonencode(local.external_secret_variables)},
    "volumesFrom": []
  }]
  DEFINITION

  tags = merge(local.tags, var.tags)
}

resource "aws_ecs_service" "default" {
  name            = local.full_name
  cluster         = aws_ecs_cluster.default.id
  task_definition = aws_ecs_task_definition.default.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = "true"
    security_groups  = [data.aws_security_group.default.id]
    subnets          = data.aws_subnet_ids.default.ids
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.default.id
    container_name   = local.full_name
    container_port   = var.port
  }

  depends_on = [data.aws_lb_listener.default]
}

resource "aws_ecr_repository" "default" {
  name = "${var.name}-${var.environment}"

  tags = merge(local.tags, var.tags)
}

resource "aws_ecr_lifecycle_policy" "limit" {
  repository = aws_ecr_repository.default.name

  policy = file("${path.module}/policies/limit.json")
}
