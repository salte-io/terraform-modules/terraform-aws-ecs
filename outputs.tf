output "zone_id" {
  value = data.aws_lb.default.zone_id
}

output "dns_name" {
  value = data.aws_lb.default.dns_name
}
