variable "name" {
  type        = string
  description = "The application name."
}

variable "environment" {
  type        = string
  description = "The current environment"
}

variable "domain_name" {
  type        = string
  description = "The domain to associate the service to"
}

variable "certificate_arn" {
  type        = string
  description = "The ARN of the Certificate in AWS"
}

variable "image_tag" {
  type        = string
  description = "The image tag for the docker image"
  default     = "latest"
}

variable "health_check" {
  type        = string
  description = "The health check path location"
  default     = "/"
}

variable "environment_variables" {
  type = map(string)
  description = "A collection of environment variables to provider to the service."
  default     = {}
}

variable "secret_variables" {
  type = map(string)
  description = "A collection of Systems Manager secret names to provide to the service."
  default     = {}
}

variable "port" {
  type        = string
  description = "The port the application runs on."
  default     = 8080
}

variable "tags" {
  type    = map(string)
  default = {}
}
